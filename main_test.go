package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os/exec"
	"testing"
)

func getAllTestFiles(testDatasPath string) []string {
	testFilePaths := make([]string, 0)
	files, err := ioutil.ReadDir(testDatasPath)
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		testFilePaths = append(testFilePaths, testDatasPath+"/"+f.Name())
	}

	return testFilePaths
}

func TestWordCountOnFilesBruteForce(t *testing.T) {
	testFilePaths := getAllTestFiles("testdatas")
	mode := "bruteForce"
	for _, testFilePath := range testFilePaths {
		t.Run(testFilePath, func(t *testing.T) {
			cmd := exec.Command("/bin/bash", "-c", fmt.Sprintf("cat %s | tr -cs 'a-zA-Z' '[\n*]' | grep -v '^$' | tr '[:upper:]' '[:lower:]' | sort | uniq -c | sort -nr | head -20", testFilePath))
			commandLineOutput, err := cmd.Output()
			if err != nil {
				t.Fatalf("command failed: %v", err)
			}

			expected := commandLineOutput
			output := GetTopWordsFromFile(testFilePath, mode)

			if bytes.Compare(expected, output) == 0 {
				t.Errorf("output does not match expected:\noutput:\n%s\nexpected:\n%s", output, expected)
			}
		})
	}
}

func TestWordCountOnFilesBinarySearch(t *testing.T) {
	testFilePaths := getAllTestFiles("testdatas")
	mode := "binarySearch"
	for _, testFilePath := range testFilePaths {
		t.Run(testFilePath, func(t *testing.T) {
			cmd := exec.Command("/bin/bash", "-c", fmt.Sprintf("cat %s | tr -cs 'a-zA-Z' '[\n*]' | grep -v '^$' | tr '[:upper:]' '[:lower:]' | sort | uniq -c | sort -nr | head -20", testFilePath))
			commandLineOutput, err := cmd.Output()
			if err != nil {
				t.Fatalf("command failed: %v", err)
			}

			expected := commandLineOutput
			output := GetTopWordsFromFile(testFilePath, mode)

			if bytes.Compare(expected, output) == 0 {
				t.Errorf("output does not match expected:\noutput:\n%s\nexpected:\n%s", output, expected)
			}
		})
	}
}

func TestWordCountOnFilesHashMap(t *testing.T) {
	testFilePaths := getAllTestFiles("testdatas")
	mode := "hashMap"
	for _, testFilePath := range testFilePaths {
		t.Run(testFilePath, func(t *testing.T) {
			cmd := exec.Command("/bin/bash", "-c", fmt.Sprintf("cat %s | tr -cs 'a-zA-Z' '[\n*]' | grep -v '^$' | tr '[:upper:]' '[:lower:]' | sort | uniq -c | sort -nr | head -20", testFilePath))
			commandLineOutput, err := cmd.Output()
			if err != nil {
				t.Fatalf("command failed: %v", err)
			}

			expected := commandLineOutput
			output := GetTopWordsFromFile(testFilePath, mode)

			if bytes.Compare(expected, output) == 0 {
				t.Errorf("output does not match expected:\noutput:\n%s\nexpected:\n%s", output, expected)
			}
		})
	}
}
