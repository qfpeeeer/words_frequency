# Word Count Project

This project counts the top 20 most frequent words in a given text file. It uses different approaches to achieve this:
brute force, binary search, and a custom hashmap
implementation. `It also checks is the file is valid text container or not.`

## Prerequisites

- Go version 1.16 or later, tested on 1.18

## How to Build and Run

1. Clone the repository:
   `git clone https://gitlab.com/qfpeeeer/words_frequency.git`
   `cd word-frequency-project`
2. Build the project:
   `go build -o word-frequency`
3. Run the project with the desired input file and mode:<br>
   `./word-frequency -file=<path_to_input_file> -mode=<bruteForce|binarySearch|hashMap>`
   <br>For example: `./word-frequency -file=testdatas/mobydick.txt -mode=hashMap`

## How to Run Tests

1. Run the tests using the following command:

`go test -v`

## How to Profile the Project

1. Build the project with the `-cpuprofile` flag:<br>
   `go build -o word-frequency`<br>
   For example: `./word-frequency -file=testdatas/mobydick.txt -mode=hashMap -cpuprofile=cpu.prof`
2. Analyze the CPU profile with `go tool pprof` in web browser:<br>
   `go tool pprof -http=:8080 cpu.prof`

## How to check the difference with linux command and code result

1. Build the project with the `go build -o word-frequency`
2. Run the project with the desired input file and mode and run
   diff:<br> `diff <(./word-frequency -file=testdatas/small_0.txt -mode="bruteForce") <(cat testdatas/small_0.txt | tr -cs 'a-zA-Z' '[\n*]' | grep -v "^$" | tr '[:upper:]' '[:lower:]'| sort | uniq -c | sort -nr | head -20)`

## Profiling Results

We have performed profiling on all the possible modes: brute force, binary search, and hashmap. <br>
The results indicate that the binary search implementation provides the best performance in terms of CPU usage, followed
by the hash map and brute force approaches.

## To-Do

In addition to the brute force, binary search, and custom hashmap approaches used in this project, I plan to add
a [Trie](https://en.wikipedia.org/wiki/Trie) data structure implementation for counting the top 20 most frequent words
in a text file. I haven't had time to work on this yet.<br>
One advantage of using a Trie over other data structures is its space efficiency. Since Trie shares common prefixes
among words, it can store a large number of words in a compact manner. This is especially useful for applications with a
large vocabulary or for cases where memory is limited.<br>
Another advantage of Trie is its fast search and insertion time. Both operations can be performed in O(k) time, where k
is the length of the word being searched or inserted. This makes Trie an ideal choice for applications that require
frequent updates or real-time responses.
