package hashmap

const defaultCapacity = 16

type Word string

type entry struct {
	key   Word
	value int
	next  *entry
}

type HashMap struct {
	buckets []*entry
	size    int
}

func hash(key Word) int {
	hash := 0
	for _, c := range key {
		hash = 31*hash + int(c)
	}
	return hash
}

func indexFor(hash, length int) int {
	return hash & (length - 1)
}

func New() *HashMap {
	return &HashMap{buckets: make([]*entry, defaultCapacity)}
}

func (m *HashMap) Put(key Word, value int) {
	hash := hash(key)
	index := indexFor(hash, len(m.buckets))

	for e := m.buckets[index]; e != nil; e = e.next {
		if e.key == key {
			e.value = value
			return
		}
	}

	newEntry := &entry{key: key, value: value, next: m.buckets[index]}
	m.buckets[index] = newEntry
	m.size++
}

func (m *HashMap) Get(key Word) (int, bool) {
	hash := hash(key)
	index := indexFor(hash, len(m.buckets))

	for e := m.buckets[index]; e != nil; e = e.next {
		if e.key == key {
			return e.value, true
		}
	}

	return 0, false
}

func (m *HashMap) Size() int {
	return m.size
}

func (m *HashMap) ForEach(f func(key Word, value int)) {
	for _, bucket := range m.buckets {
		for e := bucket; e != nil; e = e.next {
			f(e.key, e.value)
		}
	}
}
