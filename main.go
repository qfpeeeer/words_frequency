package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"os"
	"runtime/pprof"
	"sort"
	"strconv"
	"unicode"

	"project/hashmap"
)

type wordCount struct {
	Word  []byte
	Count int
}

type wordFrequency []wordCount

func isTextFile(filename string) (bool, error) {
	file, err := os.Open(filename)
	if err != nil {
		return false, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Printf("error closing file: %v", err.Error())
			os.Exit(1)
		}
	}(file)

	const bufferSize = 8192
	const minTextPercentage = 0.7

	buf := make([]byte, bufferSize)
	total := 0
	textCount := 0

	for {
		n, err := file.Read(buf)
		if err != nil && err != io.EOF {
			return false, err
		}

		if n == 0 {
			break
		}

		for i := 0; i < n; i++ {
			if buf[i] >= 32 && buf[i] <= 126 || buf[i] == '\n' || buf[i] == '\r' || buf[i] == '\t' {
				textCount++
			}
		}
		total += n
	}

	return float64(textCount)/float64(total) >= minTextPercentage, nil
}

func updateWordFrequency(words interface{}, word []byte, mode string) {
	switch mode {
	case "bruteForce":
		updateWordFrequencyBruteForce(words.(*wordFrequency), word)
	case "binarySearch":
		updateWordFrequencyBinarySearch(words.(*wordFrequency), word)
	case "hashMap":
		updateWordFrequencyHashMap(words.(*hashmap.HashMap), word)
	}
}

func updateWordFrequencyBruteForce(words *wordFrequency, word []byte) {
	index := -1
	for i, wc := range *words {
		if bytes.Compare(wc.Word, word) == 0 {
			index = i
			break
		}
	}

	if index >= 0 {
		(*words)[index].Count++
	} else {
		*words = append(*words, wordCount{Word: word, Count: 1})
	}
}

func updateWordFrequencyBinarySearch(words *wordFrequency, word []byte) {
	index := sort.Search(len(*words), func(i int) bool { return bytes.Compare((*words)[i].Word, word) >= 0 })

	if index < len(*words) && bytes.Compare((*words)[index].Word, word) == 0 {
		(*words)[index].Count++
	} else {
		*words = append(*words, wordCount{})
		copy((*words)[index+1:], (*words)[index:])
		(*words)[index] = wordCount{Word: word, Count: 1}
	}
}

func updateWordFrequencyHashMap(words *hashmap.HashMap, word []byte) {
	key := hashmap.Word(word)
	count, found := words.Get(key)
	if found {
		words.Put(key, count+1)
	} else {
		words.Put(key, 1)
	}
}

func buildWordFrequency(reader *bufio.Reader, mode string) interface{} {
	var tmp []byte
	var wordFreq interface{}

	switch mode {
	case "bruteForce", "binarySearch":
		wordFreq = new(wordFrequency)
	case "hashMap":
		wordFreq = hashmap.New()
	}

	for {
		b, err := reader.ReadByte()
		if err != nil {
			if err != io.EOF {
				fmt.Println("Error reading file: ", err)
			}
			break
		}
		if unicode.IsLetter(rune(b)) {
			tmp = append(tmp, byte(unicode.ToLower(rune(b))))
		} else {
			if tmp != nil {
				updateWordFrequency(wordFreq, tmp, mode)
				tmp = nil
			}
		}
	}

	if tmp != nil {
		updateWordFrequency(wordFreq, tmp, mode)
	}

	return wordFreq
}

func GetTopWordsFromFile(filename string, mode string) []byte {
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("Failed to open file: ", err)
		os.Exit(1)
	}
	defer func(file *os.File) {
		if err := file.Close(); err != nil {
			fmt.Println("Failed to close file: ", err)
			os.Exit(1)
		}
	}(file)

	reader := bufio.NewReader(file)
	wordFreqInterface := buildWordFrequency(reader, mode)

	var sortedWordFreq wordFrequency

	switch mode {
	case "bruteForce", "binarySearch":
		sortedWordFreq = *(wordFreqInterface.(*wordFrequency))
	case "hashMap":
		hashMapWordFreq := wordFreqInterface.(*hashmap.HashMap)
		sortedWordFreq = make(wordFrequency, 0, hashMapWordFreq.Size())
		hashMapWordFreq.ForEach(func(key hashmap.Word, count int) {
			sortedWordFreq = append(sortedWordFreq, wordCount{Word: []byte(key), Count: count})
		})
	}

	sort.Slice(sortedWordFreq, func(i, j int) bool { return sortedWordFreq[i].Count > sortedWordFreq[j].Count })

	maxLength := 20
	if maxLength > len(sortedWordFreq) {
		maxLength = len(sortedWordFreq)
	}

	var answer []byte
	for i := 0; i < maxLength; i++ {
		answer = strconv.AppendInt(answer, int64(sortedWordFreq[i].Count), 10)
		answer = append(answer, ' ')
		answer = append(answer, []byte(sortedWordFreq[i].Word)...)
		if i+1 != maxLength {
			answer = append(answer, '\n')
		}
	}

	return answer
}

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
var mode = flag.String("mode", "", "mode of updateWordFrequency use")
var filePath = flag.String("file", "", "test file path")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			fmt.Println("Failed to create file: ", err)
			os.Exit(1)
		}
		defer func(f *os.File) {
			err := f.Close()
			if err != nil {
				fmt.Println("Failed to close file: ", err)
				os.Exit(1)
			}
		}(f)
		if err := pprof.StartCPUProfile(f); err != nil {
			fmt.Println("Failed to start CPU profile: ", err)
			os.Exit(1)
		}
		defer pprof.StopCPUProfile()
	}

	if *filePath == "" {
		fmt.Println("No file path was provided")
		fmt.Println("Usage: go run main.go [-file <path_to_file>] [-mode <mode>]")
		os.Exit(1)
	}

	if isText, err := isTextFile(*filePath); !isText || err != nil {
		if err != nil {
			fmt.Println("Failed to check if file is a text file: ", err)
			os.Exit(1)
		}
		fmt.Println("File is not a text file")
		os.Exit(1)
	}

	if *mode == "" {
		fmt.Println("No mode was provided")
		fmt.Println("Usage: go run main.go [-file <path_to_file>] [-mode <mode>]\nAvailable modes: bruteForce, binarySearch, hashMap")
		os.Exit(1)
	}

	byteArray := GetTopWordsFromFile(*filePath, *mode)
	lines := make([][]byte, 0)
	start := 0
	for i, b := range byteArray {
		if b == '\n' {
			line := byteArray[start:i]
			lines = append(lines, line)
			start = i + 1
		}
	}
	if start < len(byteArray) {
		line := byteArray[start:]
		lines = append(lines, line)
	}

	for _, line := range lines {
		spaceIndex := -1
		count := 0
		for id, c := range line {
			if c == ' ' {
				spaceIndex = id
				break
			}

			if '0' <= c && c <= '9' {
				count *= 10
				count += int(c - '0')
			} else {
				fmt.Printf("[error]: at count part found not digit")
				os.Exit(1)
			}
		}

		if spaceIndex == -1 {
			fmt.Printf("[error]: seperating space is not found")
			os.Exit(1)
		}
		fmt.Printf("%7d %s\n", count, line[spaceIndex+1:])
	}
}
